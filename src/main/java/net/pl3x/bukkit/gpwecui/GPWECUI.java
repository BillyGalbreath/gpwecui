package net.pl3x.bukkit.gpwecui;

import me.ryanhamshire.GriefPrevention.Claim;
import me.ryanhamshire.GriefPrevention.events.VisualizationEvent;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.nio.charset.Charset;
import java.util.Collection;

public class GPWECUI extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        if (!getServer().getPluginManager().isPluginEnabled("GriefPrevention")) {
            getLogger().severe("# GriefPrevention NOT found and/or enabled!");
            getLogger().severe("# This plugin requires GriefPrevention to be installed and enabled!");
            return;
        }

        getServer().getMessenger().registerOutgoingPluginChannel(this, "WECUI");

        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onGPVisualize(VisualizationEvent event) {
        Player player = event.getPlayer();
        Collection<Claim> claims = event.getClaims();

        if (claims.size() > 1 || claims.size() < 1) {
            sendCUI(player, null, null, 0);
            return;
        }

        for (Claim claim : claims) {
            Location min = claim.getLesserBoundaryCorner();
            Location max = claim.getGreaterBoundaryCorner();

            max.setY(max.getWorld().getMaxHeight());

            sendCUI(player, min, max, claim.getArea());
        }
    }

    private void sendCUI(Player player, Location min, Location max, int area) {
        if (min == null || max == null) {
            sendCUI(player, "s|cuboid");
            return;
        }

        sendCUI(player, "p|0|" + min.getBlockX() + "|" + min.getBlockY() + "|" + min.getBlockZ() + "|" + area);
        sendCUI(player, "p|1|" + max.getBlockX() + "|" + max.getBlockY() + "|" + max.getBlockZ() + "|" + area);
    }

    private void sendCUI(Player player, String msg) {
        player.sendPluginMessage(this, "WECUI", msg.getBytes(Charset.forName("UTF-8")));
    }
}
